# bin #

Executable scripts go here.

## newday ##

The `newday` script can be run to create template files for a day's solutions, data and tests.

### Usage ###

`newday` should be run from the project's root directory (not from the `bin` directory).
It takes one argument: the day number.

For example, to create template files for the solution for day 5, run:

```bash
bin/newday 5
```
