# Advent of Code #

[Advent of Code](https://adventofcode.com/)

## Notes ##

The project uses a 
[Makefile](https://bitbucket.org/toadstule/workspace/snippets/zXEA4n/makefiles), that contains
a `help` target; for more info, type:

```bash
make help
```

The project should be run in a Python
[virtual environment](https://docs.python.org/3/tutorial/venv.html).
The Makefile expects the virtual environment to be located in `.venv`. It can be created using
your editor, or manually as follows:

```bash
make venv
```

Once created, the environment can be activated with:

```bash
source .venv/bin/activate
```

